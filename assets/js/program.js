const handleSubmit = (e) => {
  console.log("Estoy adentro");
    e.preventDefault()
  let myForm = document.getElementById('contacto-portfolio');
  let formData = new FormData(myForm)
  fetch('/', {
    method: 'POST',
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: new URLSearchParams(formData).toString()
  }).then(() => console.log('Form successfully submitted')).catch((error) =>
    alert(error))
}

document.querySelector("form").addEventListener("submit", handleSubmit);